# listctl

## status
- Main functionality implemented.
- Testing ...

## syntax
```
$ ./listctl 
listctl controls the List cluster manager.

 Find more information at: https://gitlab.com/lknite/list

Usage:
  listctl [flags]
  listctl [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  config      Modify listconfig files
  create      Create a list
  delete      Delete list
  get         Get list
  help        Help about any command
  label       Update the labels on a list (configure list settings)
  login       Login
  pull        Checkout block
  push        Checkin block
  resume      Resume list
  suspend     Suspend list
  token       Get token
  version     Print the version number of listctl

Flags:
  -h, --help   help for listctl

Use "listctl [command] --help" for more information about a command.
```

## listconfig ##
```
$ cat ~/.list/config 
apiVersion: "v1"
kind: Config
clusters:
- cluster:
    certificate-authority-data: ''
    server: https://list.vc-non.k.home.net
  name: vc-non
contexts:
- context:
    cluster: vc-non
    namespace: lknite
    user: vc-non-admin
  name: vc-non
current-context: vc-non
users:
- name: vc-non-admin
  user:
    exec:
      apiVersion: client.authentication.home.net/v1.0.0
      args:
      - login
      - oidc
      - --client-id=defaultwithsecret
      - --client-secret=372XBk0uLPbFhXpl3a0KNh6wgD9JotJd
      - --provider-uri=https://keycloak.vc-prod.k.home.net/realms/home
      command: /home/lknite/bin/listctl
    client-certificate-data: ''
    client-key-data: ''
```

## usage ##
```
$ ./listctl create --name demo --task something --total 12341234123412341234 --block-size 123
list/demo created

$ ./listctl get -o wide
NAME     TASK        ACTION   STATE     ACCESS   AGE   TOTAL                  SIZE
demo     something            pending   public   10h   12341234123412341234   123
```
