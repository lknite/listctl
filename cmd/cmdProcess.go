package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	"time"

	"github.com/google/uuid"
	"github.com/spf13/cobra"
)

var cmdProcess = &cobra.Command{
	Use:   "process",
	Short: "Process lists",
	Long:  `Process lists, long`,
	Run: func(c *cobra.Command, args []string) {
		//
		fmt.Printf("todo, listen to websocket for new lists to work on (no need for polling)\n")

		for {
			// reset whether active list was found, and whether a block was found
			foundActive := false
			foundBlock := false

			//
			lists := ListGet()

			for _, list := range lists {
				log.Printf("list: '%v'\n", list.Name)

				// ignore if state is not 'active'
				if list.State != "active" {
					continue
				}
				foundActive = true

				// pull block
				block := BlockGet(list.Name)

				// was pull successful?

				data, err := json.MarshalIndent(block, "", "  ")
				if err != nil {
					log.Fatalln(err)
				}
				log.Println(string(data))

				// todo: better error handling, return error if no block, instead of empty
				if block.Id == 0 {
					continue
				}
				foundBlock = true

				// process block
				//fmt.Printf("- block.Id: %v\n", block.Id)
				//fmt.Printf("- block.Key: %v\n", block.Key)
				index := getBlockBig("index", block.Id, block.Key)
				log.Printf("- big index: %v", index)

				size := getBlockBig("size", block.Id, block.Key)
				log.Printf("-  big size: %v", size)

				// push block
				BlockPut(list.Name, block)
			}

			if foundActive && !foundBlock {
				log.Printf("no blocks available, pausing a moment ...")
				time.Sleep(1 * time.Second)

				continue
			}
			if !foundActive {
				log.Printf("no active lists, exiting")

				break
			}
		}
	},
}

func BlockPut(name string, block BlockRef) {
	// api response
	var resp *http.Response

	// authentication workflow
	var auth AuthWorkflow

	// perform api call
	// - use forever loop to gather up credentials as needed
	for {
		// try next authentication method
		err := auth.ProcessNextMethod()
		if err != nil {
			// all authentication methods failed, exit gracefully
			break
		}

		// initialize rest client
		client := &http.Client{
			Timeout: time.Second * 10,
		}
		uri := listCluster.ClusterDetail.Server + "/block"
		req, err := http.NewRequest("PUT", uri, nil)
		if err != nil {
			log.Fatal(err)
		}

		// add authentication header
		auth.AddHeader(req)

		values := req.URL.Query()
		values.Add("name", name)
		values.Add("id", strconv.Itoa(block.Id))
		values.Add("key", block.Key.String())
		req.URL.RawQuery = values.Encode()

		// invoke method
		resp, err = client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()

		// if all done, break
		if resp.StatusCode == 200 {
			break
		}

		// handle errors
		switch resp.StatusCode {
		case 401:
			//log.Printf("access denied\n")
		default:
			log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
		}
	}

	/*
		// parse apiResponse
		respBody, _ := io.ReadAll(resp.Body)
		//fmt.Println("result: ", string(respBody))

		result, err := strconv.Atoi(string(respBody))
		if err != nil {
			log.Fatalln(err)
		}

		if result == 1 {
			fmt.Printf("block/%v checked in\n", args[1])
		} else {
			fmt.Printf("unknown error\n")
		}
	*/
}

type BlockRef struct {
	Id       int       `json:"id"`
	List     int       `json:"list"`
	State    string    `json:"state"`
	IndexId  int       `json:"indexId"`
	SizeId   int       `json:"sizeId"`
	Replicas int       `json:"replicas"`
	When     time.Time `json:"when"`
	Who      string    `json:"who"`
	Key      uuid.UUID `json:"key"`
}

func BlockGet(name string) BlockRef {
	// api response
	var resp *http.Response

	// authentication workflow
	var auth AuthWorkflow

	// perform api call
	// - use forever loop to gather up credentials as needed
	for {
		// try next authentication method
		err := auth.ProcessNextMethod()
		if err != nil {
			// all authentication methods failed, exit gracefully
			break
		}

		// initialize rest client
		client := &http.Client{
			Timeout: time.Second * 10,
		}
		uri := listCluster.ClusterDetail.Server + "/block"
		req, err := http.NewRequest("GET", uri, nil) //bytes.NewBuffer(json_data))
		if err != nil {
			log.Fatal(err)
		}

		// add authentication header
		auth.AddHeader(req)

		values := req.URL.Query()
		if Output != "" {
			values.Add("output", Output)
		}
		values.Add("name", name)
		req.URL.RawQuery = values.Encode()

		// invoke create method
		resp, err = client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		// if all done, break
		if resp.StatusCode == 200 {
			break
		}

		// handle errors
		switch resp.StatusCode {
		case 401:
			//log.Printf("access denied\n")
		default:
			log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
		}
	}

	// map response
	var block BlockRef

	// parse apiResponse
	respBody, _ := io.ReadAll(resp.Body)
	json.Unmarshal(respBody, &block)

	return block
}

// map response
type ListRef struct {
	Id      int       `json:"id"`
	When    time.Time `json:"when"`
	Who     string    `json:"who"`
	Name    string    `json:"name"`
	Task    string    `json:"task"`
	Action  string    `json:"action"`
	State   string    `json:"state"`
	Access  string    `json:"access"`
	TotalId int       `json:"totalId"`
	SizeId  int       `json:"sizeId"`
}

func ListGet() []ListRef {

	// api response
	var resp *http.Response

	// authentication workflow
	var auth AuthWorkflow

	// perform api call
	// - use forever loop to gather up credentials as needed
	for {
		// try next authentication method
		err := auth.ProcessNextMethod()
		if err != nil {
			// all authentication methods failed, exit gracefully
			break
		}

		// initialize rest client
		client := &http.Client{
			Timeout: time.Second * 10,
		}
		uri := listCluster.ClusterDetail.Server + "/list"
		req, err := http.NewRequest("GET", uri, nil)
		if err != nil {
			log.Fatal(err)
		}

		// add authentication header
		auth.AddHeader(req)

		// invoke method
		resp, err = client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()

		// if all done, break
		if resp.StatusCode == 200 {
			break
		}

		// handle errors
		switch resp.StatusCode {
		case 401:
			//log.Printf("access denied\n")
		default:
			log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
		}
	}

	// map response
	var lists []ListRef

	// parse apiResponse
	respBody, _ := io.ReadAll(resp.Body)
	json.Unmarshal(respBody, &lists)

	return lists
}
