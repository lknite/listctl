package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"time"

	"github.com/spf13/cobra"
)

var cmdCreateKey = &cobra.Command{
	Use:   "key",
	Short: "Create a key",
	Long:  `Create a key`,
	Run: func(c *cobra.Command, args []string) {
		// process arguments
		// - an argument is expected, or if no argument, '--name' or '-n' is expected
		if len(args) == 0 && len(Name) == 0 {
			// if no argument was provided then, we expect '--name' or '-n'
			c.Help()
			os.Exit(0)
		} else if len(args) > 0 && len(args[0]) > 0 {
			// set the argument as the name parameter, if specified
			Name = args[0]
		}

		// check parameters
		switch Output {
		case "":
			{
			}
		case "json":
			{
			}
		case "yaml":
			{
			}
		default:
			{
				log.Fatalln(`error: unable to match a printer suitable for the output format "` + Output + `", allowed formats are: json, yaml`)
			}
		}

		// api response
		var resp *http.Response

		// authentication workflow
		var auth AuthWorkflow

		// perform api call
		// - use forever loop to gather up credentials as needed
		for {
			// try next authentication method
			err := auth.ProcessNextMethod()
			if err != nil {
				// all authentication methods failed, exit gracefully
				break
			}

			// initialize rest client
			client := &http.Client{
				Timeout: time.Second * 10,
			}
			uri := listCluster.ClusterDetail.Server + "/key"
			req, err := http.NewRequest("POST", uri, nil)
			if err != nil {
				log.Fatal(err)
			}

			// add authentication header
			auth.AddHeader(req)

			values := req.URL.Query()
			if Name != "" {
				values.Add("name", Name)
			}
			req.URL.RawQuery = values.Encode()

			// invoke method
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()

			// if all done, break
			if resp.StatusCode == 200 {
				break
			}

			// handle errors
			switch resp.StatusCode {
			case 401:
				//log.Printf("access denied\n")
			default:
				log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
			}
		}

		/*
			// map response
			type ListRef struct {
				Id      int       `json:"id"`
				When    time.Time `json:"when"`
				Who     string    `json:"who"`
				Name    string    `json:"name"`
				Task    string    `json:"task"`
				Action  string    `json:"action"`
				State   string    `json:"state"`
				Access  string    `json:"access"`
				TotalId int       `json:"totalId"`
				SizeId  int       `json:"sizeId"`
			}
			var response []ListRef

			// parse apiResponse
			respBody, _ := io.ReadAll(resp.Body)
			json.Unmarshal(respBody, &response)

			//
			switch Output {
			case "":
				//
				w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0) //, tabwriter.AlignRight|tabwriter.Debug)
				fmt.Fprintln(w, strings.Join([]string{"NAME", "TASK", "ACTION", "STATE", "ACCESS", "AGE"}, "\t"))
				for _, d := range response {
					fmt.Fprintln(w, strings.Join([]string{d.Name, d.Task, d.Action, d.State, d.Access, "10h"}, "\t"))
				}
				w.Flush()
			case "wide":
				//
				w := tabwriter.NewWriter(os.Stdout, 0, 0, 3, ' ', 0) //, tabwriter.AlignRight|tabwriter.Debug)
				fmt.Fprintln(w, strings.Join([]string{"NAME", "TASK", "ACTION", "STATE", "ACCESS", "AGE", "TOTAL", "SIZE"}, "\t"))
				for _, d := range response {

					total := getListBig(tokens, "total", d.Id)
					size := getListBig(tokens, "size", d.Id)

					fmt.Fprintln(w, strings.Join([]string{d.Name, d.Task, d.Action, d.State, d.Access, "10h", TruncateString(total, 25), TruncateString(size, 25)}, "\t"))
				}
				w.Flush()
			case "json":
				data, err := json.MarshalIndent(response, "", "  ")
				if err != nil {
					log.Fatalln(err)
				}
				fmt.Println(string(data))
			case "yaml":
				data, err := yaml.Marshal(response)
				if err != nil {
					log.Fatalln(err)
				}
				fmt.Println(string(data))
			}
		*/

		// display result
		respBody, _ := io.ReadAll(resp.Body)
		fmt.Println(string(respBody))
	},
}
