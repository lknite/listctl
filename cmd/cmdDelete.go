package main

import (
	"os"

	"github.com/spf13/cobra"
)

var cmdDelete = &cobra.Command{
	Use:   "delete",
	Short: "Delete resources",
	Long:  `Delete resources, long`,
	Run: func(c *cobra.Command, args []string) {
		c.Help()
		os.Exit(0)
	},
}

/*
		// api response
		var resp *http.Response

		// authentication workflow
		var auth AuthWorkflow

		// perform api call
		// - use forever loop to gather up credentials as needed
		for {
			// try next authentication method
			err := auth.ProcessNextMethod()
			if err != nil {
				// all authentication methods failed, exit gracefully
				break
			}

			// initialize rest client
			client := &http.Client{
				Timeout: time.Second * 10,
			}
			uri := listCluster.ClusterDetail.Server + "/list"
			req, err := http.NewRequest("DELETE", uri, nil) //bytes.NewBuffer(json_data))
			if err != nil {
				log.Fatal(err)
			}

			// add authentication header
			auth.AddHeader(req)

			values := req.URL.Query()
			Name = args[0] // name is now a required arg, rather than flag
			if Name != "" {
				values.Add("name", Name)
			}
			req.URL.RawQuery = values.Encode()

			// invoke create method
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()
			// if all done, break
			if resp.StatusCode == 200 {
				break
			}

			// handle errors
			switch resp.StatusCode {
			case 401:
				//log.Printf("access denied\n")
			default:
				log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
			}
		}

		respBody, _ := io.ReadAll(resp.Body)
		if string(respBody) == "ok" {
			fmt.Printf("list/%v deleted\n", Name)
		} else {
			fmt.Printf("not authorized to perform action\n")
		}
	},
}
*/
