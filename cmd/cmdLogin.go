package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"time"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/spf13/cobra"
	"golang.org/x/oauth2"
)

var cmdLogin = &cobra.Command{
	Use:   "login",
	Short: "Login",
	Long:  `Login, http://127.0.0.1:8080/login`,
	Run: func(cmd *cobra.Command, args []string) {
		//fmt.Println("to login browse to http://127.0.0.1:8080/login")
		handleRequests()
	},
}

func randString(nByte int) (string, error) {
	b := make([]byte, nByte)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(b), nil
}

func setCallbackCookie(w http.ResponseWriter, r *http.Request, name, value string) {
	c := &http.Cookie{
		Name:     name,
		Value:    value,
		MaxAge:   int(time.Hour.Seconds()),
		Secure:   r.TLS != nil,
		HttpOnly: true,
	}
	http.SetCookie(w, c)
}

/*
// attempt to acquire access token using refresh token
func Refresh(refreshToken string) error {
	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, ProviderUri)
	if err != nil {
		log.Fatalln(err)
	}

	config := oauth2.Config{
		ClientID:     ClientId,
		ClientSecret: ClientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  "", // set below, e.g. "http://127.0.0.1:8080/callback",
	}

	// set refresh token
	ts := config.TokenSource(ctx, &oauth2.Token{RefreshToken: refreshToken})

	// attempt to refresh access token
	oauth2Token, err := ts.Token()
	if err != nil {
		return err
	}

	// pretty-print json
	data, err := json.MarshalIndent(oauth2Token, "", "  ")
	if err != nil {
		return err
	}

	// save acquired access and refresh tokens
	err = os.WriteFile(listdir+"/cache/tokens", []byte(data), 0600)
	if err != nil {
		log.Fatalln(err)
	}

	// wait a moment then exit, so the write has a chance to flush
	time.Sleep(100 * time.Millisecond)

	return nil
}
*/

func handleRequests() {
	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, ProviderUri)
	if err != nil {
		log.Fatalln(err)
	}

	oidcConfig := &oidc.Config{
		ClientID: ClientId,
	}
	verifier := provider.Verifier(oidcConfig)

	config := oauth2.Config{
		ClientID:     ClientId,
		ClientSecret: ClientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  "", // set below, e.g. "http://127.0.0.1:8080/callback",
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}

	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		state, err := randString(16)
		if err != nil {
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		nonce, err := randString(16)
		if err != nil {
			http.Error(w, "Internal error", http.StatusInternalServerError)
			return
		}
		setCallbackCookie(w, r, "state", state)
		setCallbackCookie(w, r, "nonce", nonce)

		http.Redirect(w, r, config.AuthCodeURL(state, oidc.Nonce(nonce)), http.StatusFound)
	})

	http.HandleFunc("/callback", func(w http.ResponseWriter, r *http.Request) {
		state, err := r.Cookie("state")
		if err != nil {
			http.Error(w, "state not found", http.StatusBadRequest)
			return
		}
		if r.URL.Query().Get("state") != state.Value {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}

		oauth2Token, err := config.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
			return
		}
		rawIDToken, ok := oauth2Token.Extra("id_token").(string)
		if !ok {
			http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)
			return
		}
		idToken, err := verifier.Verify(ctx, rawIDToken)
		if err != nil {
			http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)
			return
		}

		nonce, err := r.Cookie("nonce")
		if err != nil {
			http.Error(w, "nonce not found", http.StatusBadRequest)
			return
		}
		if idToken.Nonce != nonce.Value {
			http.Error(w, "nonce did not match", http.StatusBadRequest)
			return
		}

		//oauth2Token.AccessToken = "*REDACTED*"

		/*
			resp := struct {
				OAuth2Token   *oauth2.Token
				IDTokenClaims *json.RawMessage // ID Token payload is just JSON.
			}{oauth2Token, new(json.RawMessage)}

			if err := idToken.Claims(&resp.IDTokenClaims); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			data, err := json.MarshalIndent(resp, "", "    ")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		*/

		// pretty-print json
		data, err := json.MarshalIndent(oauth2Token, "", "  ")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// save acquired access and refresh tokens
		err = os.WriteFile(listdir+"/cache/tokens", []byte(data), 0600)
		if err != nil {
			log.Fatalln(err)
		}

		w.Write([]byte("auth successful, close tab to continue"))

		// wait a moment then exit, so the write has a chance to flush
		go func() {
			//
			time.Sleep(100 * time.Millisecond)

			// exit, oidc attempt complete
			os.Exit(0)
		}()
	})

	// get next available port to listen on
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		panic(err)
	}
	// update callback url with the randomly select port
	config.RedirectURL = fmt.Sprintf("http://%v/callback", listener.Addr())
	// update login url and output for client use
	fmt.Printf("http://%v/login\n", listener.Addr())

	// start listener
	log.Fatal(http.Serve(listener, nil))
}
