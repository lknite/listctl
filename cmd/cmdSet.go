package main

import (
	"os"

	"github.com/spf13/cobra"
)

var cmdSet = &cobra.Command{
	Use:   "set",
	Short: "Set resource attributes",
	Long:  `Set resource attributes, long`,
	Run: func(c *cobra.Command, args []string) {
		c.Help()
		os.Exit(0)
	},
}
