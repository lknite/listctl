package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"gopkg.in/yaml.v3"
)

/**
 * global variables
 */

// folders
var listdir string
var cachedir string

// config
var config Config
var configContext Context
var configCluster Cluster
var configUser User

// parameter values
var List string
var Total string
var Size string
var Name string
var Task string
var Action string
var Output string
var Key string
var All bool

var ClientId string
var ClientSecret string
var ProviderUri string

func main() {
	// initialize log
	log.SetPrefix(os.Args[0] + ": ")
	log.SetFlags(0)

	// acquire/initialize listdir path
	homedir, err := os.UserHomeDir()
	if err != nil {
		log.Fatalln(err)
	}
	// ensure '~/.list' dir exists
	appdir = filepath.Join(homedir, ".list")
	err = os.MkdirAll(appdir, os.ModePerm)
	if err != nil {
		log.Fatalln(err)
	}
	// ensure 'cache' dir exists
	cachedir = filepath.Join(appdir, "cache")
	err = os.MkdirAll(cachedir, 0700)
	if err != nil {
		log.Fatalln(err)
	}

	// load up config file
	configpath := filepath.Join(appdir, "config")
	raw, err := os.ReadFile(configpath)
	if err != nil {
		log.Fatalln(err)
	}
	// raw data -> struct
	err = yaml.Unmarshal(raw, &config)
	if err != nil {
		log.Fatalln(err)
	}

	// search config for current context values
	for _, _context := range config.Contexts {
		if strings.EqualFold(strings.ToLower(_context.Name), strings.ToLower(config.CurrentContext)) {
			listContext = _context
			// config: locate current cluster
			for _, _cluster := range config.Clusters {
				listCluster = _cluster
				break
			}
			// config: locate current user
			for _, _user := range config.Users {
				listUser = _user
				break
			}

			break
		}
	}

	// subcommands
	cmdLogin.Flags().StringVarP(&ClientId, "client-id", "", "", "oidc client id")
	cmdLogin.MarkFlagRequired("client-id")
	cmdLogin.Flags().StringVarP(&ClientSecret, "client-secret", "", "", "oidc client secret")
	cmdLogin.MarkFlagRequired("client-secret")
	cmdLogin.Flags().StringVarP(&ProviderUri, "provider-uri", "", "", "oidc provider uri")
	cmdLogin.MarkFlagRequired("provider-uri")

	cmdCreateKey.Flags().StringVarP(&Name, "name", "n", "", "Name of key")
	//cmdCreateKey.MarkFlagRequired("name")

	cmdCreateList.Flags().StringVarP(&Name, "name", "n", "", "Name of list")
	cmdCreateList.MarkFlagRequired("name")
	cmdCreateList.Flags().StringVarP(&Total, "total", "t", "", "Total number of tasks to perform")
	cmdCreateList.MarkFlagRequired("total")
	cmdCreateList.Flags().StringVarP(&Size, "block-size", "b", "", "Size of block handed out on a push/pull")
	cmdCreateList.MarkFlagRequired("block-size")
	cmdCreateList.Flags().StringVarP(&Task, "task", "k", "", "List task")
	cmdCreateList.MarkFlagRequired("task")
	cmdCreateList.Flags().StringVarP(&Action, "action", "a", "", "Action to perform with each item in the list")
	cmdCreateList.MarkFlagRequired("action")

	cmdGetKey.Flags().StringVarP(&Output, "output", "o", "", "Output format. One of: (json, yaml, wide).")
	cmdGetKey.Flags().BoolVarP(&All, "all", "a", false, "get all keys")
	cmdGetList.Flags().StringVarP(&Output, "output", "o", "", "Output format. One of: (json, yaml, wide).")
	cmdGetList.Flags().BoolVarP(&All, "all", "a", false, "get all lists")
	cmdGetServer.Flags().StringVarP(&Output, "output", "o", "", "Output format. One of: (json, yaml, wide).")

	cmdDeleteList.SetUsageTemplate("Usage:\n  listctl delete list [name] [flags]\n" +
		"\n" +
		"Flags:\n" +
		"  -h, --help   help for process\n" +
		"  -a, --all    delete all lists\n")
	cmdDeleteList.Flags().BoolVarP(&All, "all", "a", false, "delete all lists")
	cmdDeleteKey.SetUsageTemplate("Usage:\n  listctl delete key [name] [flags]\n" +
		"\n" +
		"Flags:\n" +
		"  -h, --help   help for process\n" +
		"  -a, --all    delete all keys\n")
	cmdDeleteKey.Flags().BoolVarP(&All, "all", "a", false, "delete all keys")

	cmdPull.SetUsageTemplate("Usage:\n  listctl pull <name> [flags]\n" +
		"\n" +
		"Flags:\n  -h, --help   help for pull\n")
	cmdPush.SetUsageTemplate("Usage:\n  listctl push <name> <id> <key> [flags]\n" +
		"\n" +
		"Flags:\n  -h, --help   help for push\n")
	cmdProcess.SetUsageTemplate("Usage:\n  listctl process <name> [flags]\n" +
		"\n" +
		"Flags:\n  -h, --help   help for process\n")

	//cmdKey.Flags().StringVarP(&Output, "output", "o", "", "Output format. One of: (json, yaml).")

	//pullCmd.Flags().StringVarP(&Name, "name", "n", "", "Name of list")
	//pullCmd.MarkFlagRequired("name")
	//pullCmd.Flags().StringVarP(&List, "list", "l", "", "ListId")
	//pullCmd.MarkFlagsOneRequired("name", "list")
	//pullCmd.MarkFlagsMutuallyExclusive("name", "list")

	// initialize cobra commandline parameters
	cmdRoot.AddCommand(cmdVersion)
	cmdRoot.AddCommand(cmdCreate)
	cmdRoot.AddCommand(cmdDelete)
	cmdRoot.AddCommand(cmdGet)
	cmdRoot.AddCommand(cmdSet)
	//cmdRoot.AddCommand(cmdPull)
	//cmdRoot.AddCommand(cmdPush)
	cmdRoot.AddCommand(cmdSuspend)
	cmdRoot.AddCommand(cmdResume)
	cmdRoot.AddCommand(cmdLogin)
	cmdRoot.AddCommand(cmdConfig)
	cmdRoot.AddCommand(cmdProcess)

	cmdCreate.AddCommand(cmdCreateList)
	cmdCreate.AddCommand(cmdCreateKey)

	cmdDelete.AddCommand(cmdDeleteList)
	cmdDelete.AddCommand(cmdDeleteKey)

	cmdGet.AddCommand(cmdGetList)
	cmdGet.AddCommand(cmdGetKey)
	cmdGet.AddCommand(cmdGetServer)

	cmdSet.AddCommand(cmdSetServer)

	// run cobra
	if err := cmdRoot.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
