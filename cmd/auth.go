package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/go-cmd/cmd"
	"golang.org/x/oauth2"

	"time"
)

//type AuthWorkflow interface {
//	New() authWorkflow
//}

const (
	AUTH_WORKFLOW_KEY = iota + 1
	AUTH_WORKFLOW_ACCESS_TOKEN
	AUTH_WORKFLOW_REFRESH_TOKEN
	AUTH_WORKFLOW_LOGIN
	AUTH_WORKFLOW_EXIT
)

type AuthWorkflow struct {
	// authentication workflow
	next int

	//
	tokens Tokens

	// auth related
	key   string
	isKey bool
}

/*
func NewAuthWorkflow() *AuthWorkflow {
	auth := new(AuthWorkflow)

	// initialize with zero, process will advanced to 1, the first method
	auth.next = 0

	return auth
}
*/

// access methods
func (auth *AuthWorkflow) GetKey() (key string) {
	return auth.key
}
func (auth *AuthWorkflow) GetTokenType() (key string) {
	return auth.tokens.Token_type
}
func (auth *AuthWorkflow) GetAccessToken() (key string) {
	return auth.tokens.Access_token
}
func (auth *AuthWorkflow) GetRefreshToken() (key string) {
	return auth.tokens.Refresh_token
}

// check current auth method
func (auth *AuthWorkflow) IsKey() bool {
	return auth.next == AUTH_WORKFLOW_KEY
}
func (auth *AuthWorkflow) IsAccessToken() bool {
	return auth.next == AUTH_WORKFLOW_ACCESS_TOKEN
}
func (auth *AuthWorkflow) IsRefreshToken() bool {
	return auth.next == AUTH_WORKFLOW_REFRESH_TOKEN
}
func (auth *AuthWorkflow) IsLogin() bool {
	return auth.next == AUTH_WORKFLOW_LOGIN
}
func (auth *AuthWorkflow) IsExit() bool {
	return auth.next == AUTH_WORKFLOW_EXIT
}

// with each call advance to next auth method
func (auth *AuthWorkflow) ProcessNextMethod() error {
	// advanced to next auth method
	auth.next++
	//log.Println("ProcessNextMethod: ", auth.next)

	//
	if auth.IsKey() {
		// check if a key exists in the environment
		auth.key, auth.isKey = os.LookupEnv("LIST_KEY")
	} else if auth.IsAccessToken() {
		// load up access token from cache, or if no cache, login
		for {
			configpath := filepath.Join(cachedir, "tokens")
			raw, err := os.ReadFile(configpath)
			if err != nil {
				// no cached tokens, perform first login
				log.Println("no cached tokens, perform first login")
				Login()

				continue
			}

			// map token values
			json.Unmarshal(raw, &auth.tokens)
			break
		}
	} else if auth.IsRefreshToken() {
		// token expired or otherwise failed, try refresh token
		Refresh(auth.GetRefreshToken())
	} else if auth.IsLogin() {
		// refresh token failed, try login
		Login()
	} else if auth.IsExit() {
		// all authentication methods failed, exit gracefully
		return errors.New("server unavailable or access denied")
	}

	return nil
}

// inserts required authorization header with either api key or bearer token
func (auth *AuthWorkflow) AddHeader(req *http.Request) {
	if auth.IsKey() {
		//log.Println("using key")
		req.Header.Add("API-KEY", auth.GetKey())
	} else {
		//log.Println("using bearer token")
		req.Header.Add("Authorization", auth.GetTokenType()+" "+auth.GetAccessToken())
	}
}

// starts up callback & launches web browser to collect credentials
func Login() {
	log.Println("login")

	// start program which will catch oidc callback (runs in the background)
	callback := cmd.NewCmd(listUser.UserDetail.Exec.Command, listUser.UserDetail.Exec.Args...)
	log.Println("start listctl as callback")
	callbackStatusChan := callback.Start()

	// declare variables which let us check stdout of callback program
	status := callback.Status()
	n := len(status.Stdout)

	// wait for first line of standard out, we will pass this to the browser
	ticker := time.NewTicker(100 * time.Millisecond)
	for range ticker.C {
		status = callback.Status()
		n = len(status.Stdout)
		//fmt.Println("n: ", n)

		// we have the url to pass to the browser
		if n > 0 {
			log.Printf("%v\n", status.Stdout[n-1])
			break
		}
	}

	// start up browser for oidc login
	log.Println("start browser")
	browser := cmd.NewCmd("xdg-open", status.Stdout[n-1])
	s := browser.Start()
	_ = s

	// now wait until the callback program exits, it will exit after one callback attempt
	log.Println("wait for callback to exit")
	finalStatus := <-callbackStatusChan
	_ = finalStatus
	log.Println("- exited")
	fmt.Println("")

	// debug
	//time.Sleep(20 * time.Second)
}

// attempt to acquire access token via refresh token (doesn't require any interaction)
func Refresh(refreshToken string) error {
	log.Println("refresh")

	// acquire provider uri from config file
	var ProviderUri string
	for _, arg := range listUser.UserDetail.Exec.Args {
		if strings.HasPrefix(arg, "--provider-uri") {
			parts := strings.Split(arg, "=")
			ProviderUri = parts[1]
		}
	}

	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, ProviderUri)
	if err != nil {
		return err
	}

	config := oauth2.Config{
		ClientID:     ClientId,
		ClientSecret: ClientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  "", // set below, e.g. "http://127.0.0.1:8080/callback",
	}

	// set refresh token
	ts := config.TokenSource(ctx, &oauth2.Token{RefreshToken: refreshToken})

	// attempt to refresh access token
	oauth2Token, err := ts.Token()
	if err != nil {
		return err
	}

	// pretty-print json
	data, err := json.MarshalIndent(oauth2Token, "", "  ")
	if err != nil {
		return err
	}

	// save acquired access and refresh tokens
	err = os.WriteFile(listdir+"/cache/tokens", []byte(data), 0600)
	if err != nil {
		return err
	}

	// wait a moment then exit, so the write has a chance to flush
	time.Sleep(100 * time.Millisecond)

	return nil
}
