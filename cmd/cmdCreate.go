package main

import (
	"os"

	"github.com/spf13/cobra"
)

var cmdCreate = &cobra.Command{
	Use:   "create",
	Short: "Create resource",
	Long:  `Create resource, long`,
	Run: func(c *cobra.Command, args []string) {
		c.Help()
		os.Exit(0)
	},
}

/*

		// api response
		var resp *http.Response

		// authentication workflow
		var auth AuthWorkflow

		// perform api call
		// - use forever loop to gather up credentials as needed
		for {
			// try next authentication method
			err := auth.ProcessNextMethod()
			if err != nil {
				// all authentication methods failed, exit gracefully
				break
			}

			// prep post body
			type Data struct {
				Name   string `json:"name"`
				Task   string `json:"task"`
				Action string `json:"action"`
				Total  string `json:"total"`
				Size   string `json:"size"`
			}
			var data Data

			// prep post body data
			data.Name = Name
			data.Task = Task
			data.Action = Action
			data.Total = Total
			data.Size = Size

			// convert struct to json
			json_data, err := json.Marshal(data)
			if err != nil {
				log.Fatal(err)
			}

			// initialize rest client
			client := &http.Client{
				Timeout: time.Second * 10,
			}
			uri := listCluster.ClusterDetail.Server + "/list"
			req, err := http.NewRequest("POST", uri, bytes.NewBuffer(json_data))
			if err != nil {
				log.Fatal(err)
			}

			// add authentication header
			auth.AddHeader(req)

			// invoke create method
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()

			// if all done, break
			if resp.StatusCode == 200 {
				break
			}

			// handle errors
			switch resp.StatusCode {
			case 401:
				//log.Printf("access denied\n")
			default:
				log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
			}
		}

		fmt.Printf("list/%v created\n", Name)
	},
}
*/
