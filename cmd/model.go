package main

// structure of cache file
type Tokens struct {
	Access_token  string `json:"access_token"`
	Token_type    string `json:"token_type"`
	Refresh_token string `json:"refresh_token"`
	Expiry        string `json:"expiry"`
}

/**
 * structure of config file
 */
type ClusterDetail struct {
	CertificateAuthorityData string `yaml:"certificate-authority-data"`
	Server                   string `yaml:"server"`
}

type Cluster struct {
	Name          string        `yaml:"name"`
	ClusterDetail ClusterDetail `yaml:"cluster"`
}

type ContextDetail struct {
	Cluster   string `yaml:"cluster"`
	Namespace string `yaml:"namespace"`
	User      string `yaml:"user"`
}

type Context struct {
	Name          string        `yaml:"name"`
	ContextDetail ContextDetail `yaml:"context"`
}

type Exec struct {
	ApiVersion string   `yaml:"apiVersion"`
	Args       []string `yaml:"args"`
	Command    string   `yaml:"command"`
}

type UserDetail struct {
	Exec                  Exec   `yaml:"exec"`
	ClientCertificateData string `yaml:"client-certificate-data"`
	ClientKeyData         string `yaml:"client-key-data"`
}

type User struct {
	Name       string     `yaml:"name"`
	UserDetail UserDetail `yaml:"user"`
}

type Config struct {
	ApiVersion     string    `yaml:"apiVersion"`
	Kind           string    `yaml:"kind"`
	Clusters       []Cluster `yaml:"clusters"`
	Contexts       []Context `yaml:"contexts"`
	CurrentContext string    `yaml:"current-context"`
	Users          []User    `yaml:"users"`
}
