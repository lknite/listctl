package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var cmdConfig = &cobra.Command{
	Use:   "config",
	Short: "Modify listconfig files",
	Long:  `Modify listconfig files, long`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Config")
	},
}
