package main

import (
	"os"

	"github.com/spf13/cobra"
)

var cmdRoot = &cobra.Command{
	Use:   "listctl",
	Short: "listctl is a List(tm) client",
	Long: `listctl controls the List cluster manager.

 Find more information at: https://gitlab.com/lknite/list`,
	Run: func(c *cobra.Command, args []string) {
		/*
			// if no arguments passed in then display the help syntax
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}
		*/
		c.Help()
		os.Exit(0)
	},
}
