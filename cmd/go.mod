module github.com/lknite/listctl

go 1.22.1

require (
	github.com/coreos/go-oidc/v3 v3.9.0
	github.com/go-cmd/cmd v1.4.2
	github.com/google/uuid v1.6.0
	github.com/spf13/cobra v1.8.0
	golang.org/x/oauth2 v0.13.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-jose/go-jose/v3 v3.0.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
