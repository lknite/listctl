package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/spf13/cobra"
)

var cmdPull = &cobra.Command{
	Use:   "pull",
	Short: "Checkout block",
	Long:  `Checkout block, long`,
	Args:  cobra.ExactArgs(1),
	//Example: "  listctl pull <name> -o json",
	Run: func(cmd *cobra.Command, args []string) {
		// check parameters
		switch Output {
		case "":
			{
			}
		case "wide":
			{
			}
		case "json":
			{
			}
		case "yaml":
			{
			}
		default:
			{
				log.Fatalln(`error: unable to match a printer suitable for the output format "` + Output + `", allowed formats are: json, yaml, wide`)
			}
		}

		// api response
		var resp *http.Response

		// authentication workflow
		var auth AuthWorkflow

		// perform api call
		// - use forever loop to gather up credentials as needed
		for {
			// try next authentication method
			err := auth.ProcessNextMethod()
			if err != nil {
				// all authentication methods failed, exit gracefully
				break
			}

			// initialize rest client
			client := &http.Client{
				Timeout: time.Second * 10,
			}
			uri := listCluster.ClusterDetail.Server + "/block"
			req, err := http.NewRequest("GET", uri, nil) //bytes.NewBuffer(json_data))
			if err != nil {
				log.Fatal(err)
			}

			// add authentication header
			auth.AddHeader(req)

			values := req.URL.Query()
			if Output != "" {
				values.Add("output", Output)
			}
			Name = args[0] // name is now a required arg, rather than flag
			if Name != "" {
				values.Add("name", Name)
			}
			req.URL.RawQuery = values.Encode()

			// invoke create method
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()
			// if all done, break
			if resp.StatusCode == 200 {
				break
			}

			// handle errors
			switch resp.StatusCode {
			case 401:
				//log.Printf("access denied\n")
			default:
				log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
			}
		}

		// map response
		type BlockRef struct {
			Id       int       `json:"id"`
			List     int       `json:"list"`
			State    string    `json:"state"`
			IndexId  int       `json:"indexId"`
			SizeId   int       `json:"sizeId"`
			Replicas int       `json:"replicas"`
			When     time.Time `json:"when"`
			Who      string    `json:"who"`
			Key      uuid.UUID `json:"key"`
		}
		var block BlockRef

		// parse apiResponse
		respBody, _ := io.ReadAll(resp.Body)
		json.Unmarshal(respBody, &block)

		data, err := json.MarshalIndent(block, "", "  ")
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(string(data))

		index := getBlockBig("index", block.Id, block.Key)
		fmt.Println("index: ", index)
		size := getBlockBig("size", block.Id, block.Key)
		fmt.Println("size: ", size)
	},
}

func getBlockBig(t string, id int, key uuid.UUID) string {
	// api response
	var resp *http.Response

	// authentication workflow
	var auth AuthWorkflow

	// perform api call
	// - use forever loop to gather up credentials as needed
	for {
		// try next authentication method
		err := auth.ProcessNextMethod()
		if err != nil {
			// all authentication methods failed, exit gracefully
			break
		}

		// initialize rest client
		client := &http.Client{
			Timeout: time.Second * 10,
		}
		uri := listCluster.ClusterDetail.Server + "/block/big"
		req, err := http.NewRequest("GET", uri, nil)
		if err != nil {
			log.Fatal(err)
		}

		// add authentication header
		auth.AddHeader(req)

		// tack on id of big
		values := req.URL.Query()
		values.Add("type", t)
		values.Add("key", key.String())
		values.Add("id", strconv.Itoa(id))
		req.URL.RawQuery = values.Encode()

		// invoke create method
		resp, err = client.Do(req)
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()

		// if all done, break
		if resp.StatusCode == 200 {
			break
		}

		// handle errors
		switch resp.StatusCode {
		case 401:
			//log.Printf("access denied\n")
		default:
			log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
		}
	}

	// read the response body into bytes
	respBody, _ := io.ReadAll(resp.Body)

	return string(respBody)
}
