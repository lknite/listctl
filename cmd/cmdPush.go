package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/spf13/cobra"
)

var cmdPush = &cobra.Command{
	Use:   "push",
	Short: "Checkin block",
	Long:  `Checkin block, long`,
	Args:  cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		// check parameters
		// - using 'Args:  cobra.ExactArgs(3),' above

		// api response
		var resp *http.Response

		// authentication workflow
		var auth AuthWorkflow

		// perform api call
		// - use forever loop to gather up credentials as needed
		for {
			// try next authentication method
			err := auth.ProcessNextMethod()
			if err != nil {
				// all authentication methods failed, exit gracefully
				break
			}

			// initialize rest client
			client := &http.Client{
				Timeout: time.Second * 10,
			}
			uri := listCluster.ClusterDetail.Server + "/block"
			req, err := http.NewRequest("PUT", uri, nil)
			if err != nil {
				log.Fatal(err)
			}

			// add authentication header
			auth.AddHeader(req)

			values := req.URL.Query()
			Name = args[0] // name is now a required arg, rather than flag
			if Name != "" {
				values.Add("name", Name)
			}
			values.Add("id", args[1])
			values.Add("key", args[2])
			req.URL.RawQuery = values.Encode()

			// invoke method
			resp, err = client.Do(req)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()

			// if all done, break
			if resp.StatusCode == 200 {
				break
			}

			// handle errors
			switch resp.StatusCode {
			case 401:
				//log.Printf("access denied\n")
			default:
				log.Printf("resp.StatusCode: %v\n", resp.StatusCode)
			}
		}

		// parse apiResponse
		respBody, _ := io.ReadAll(resp.Body)
		//fmt.Println("result: ", string(respBody))

		result, err := strconv.Atoi(string(respBody))
		if err != nil {
			log.Fatalln(err)
		}

		if result == 1 {
			fmt.Printf("block/%v checked in\n", args[1])
		} else {
			fmt.Printf("unknown error\n")
		}
	},
}
