package auth

import (
	"os"
)

type AuthWorkflow interface {
	New() authWorkflow
}

const (
	AUTH_WORKFLOW_KEY = iota
	AUTH_WORKFLOW_ACCESS_TOKEN
	AUTH_WORKFLOW_REFRESH_TOKEN
	AUTH_WORKFLOW_LOGIN
	AUTH_WORKFLOW_EXIT
)

type authWorkflow struct {
	// authentication workflow
	next int

	// auth related
	key   string
	isKey bool
}

func New() authWorkflow {
	var auth authWorkflow
	auth.next = AUTH_WORKFLOW_KEY

	// check if a key exists in the environment
	auth.key, auth.isKey = os.LookupEnv("LIST_KEY")

	return auth
}

func (auth *authWorkflow) GetNextAuthMethod() int {
	auth.next++
	return auth.next
}
func (auth *authWorkflow) GetKey() (key string) {
	return auth.key
}

func (auth *authWorkflow) IsKey() bool {
	return auth.next == AUTH_WORKFLOW_KEY
}
func (auth *authWorkflow) IsAccessToken() bool {
	return auth.next == AUTH_WORKFLOW_ACCESS_TOKEN
}
func (auth *authWorkflow) IsRefreshToken() bool {
	return auth.next == AUTH_WORKFLOW_REFRESH_TOKEN
}
func (auth *authWorkflow) IsLogin() bool {
	return auth.next == AUTH_WORKFLOW_LOGIN
}
func (auth *authWorkflow) IsExit() bool {
	return auth.next == AUTH_WORKFLOW_EXIT
}
